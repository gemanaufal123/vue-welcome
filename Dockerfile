FROM node:latest

WORKDIR /app

COPY /package.json .

RUN npm install

# RUN npm run dev

RUN npm run build

COPY . .

EXPOSE 3010

# CMD ["npm run start"]

CMD ["node", "run","start"]
